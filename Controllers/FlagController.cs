﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MultiLingual.Models;

namespace MultiLingual.Controllers
{
    public class FlagController : Controller
    {
        private readonly Microsoft.Extensions.Localization.IStringLocalizer<FlagController> _localizer;
        public FlagController(Microsoft.Extensions.Localization.IStringLocalizer<FlagController> localizer)
        {
            _localizer = localizer;
        }
       
        public IActionResult Entertainment()
        {
        ViewBag.Message = "This is the list of latest great songs:-";
            var flags = new List<Flags>()
            {
                 new Flags{Sname = _localizer["Dont Let me down"]},
                 new Flags{Sname = _localizer["Lagdi Lahore Di"]},
                 new Flags{Sname = _localizer["On my way"]}

            };
            return View(flags);
        }
        public IActionResult Entertainment2()
        {
            return View();
        }
    }
}